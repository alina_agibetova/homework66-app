import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryItemComponent } from './country-list/country-item/country-item.component';
import { CountryResolverService } from './country-list/country-resolver.service';

const routes: Routes = [
  {path: ':alpha3Code', component: CountryItemComponent,
  resolve: {
    country: CountryResolverService
  }},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
