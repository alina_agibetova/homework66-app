export class Country {
  constructor(
    public alpha3Code: string,
    public name: string,
    public capital: string,
    public population: string,
    public region: string,
    public flag: string,
  ){}
}
