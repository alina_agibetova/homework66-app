import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Country } from './country.model';
import { map } from 'rxjs/operators';

@Injectable()
export class CountryService {
  countryChange = new Subject<Country[]>();
  countryFetching = new Subject<boolean>();
  private countries: Country[] = [];

  constructor(private http: HttpClient){}

  fetchCountries(){
    this.countryFetching.next(true);
    this.http.get<{[country:string]: Country}>(`http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name;alpha3Code`)
      .pipe(map(result => {
        return Object.keys(result).map(country =>{
          const countryData = result[country];

          return new Country(countryData.alpha3Code, countryData.name, countryData.capital, countryData.population, countryData.region, countryData.flag);
        })
      }))
      .subscribe(countries => {
        this.countries = countries;
        this.countryChange.next(this.countries.slice());
        this.countryFetching.next(false);
      }, error => {
        this.countryFetching.next(false);
      });


  }

  getCountries(){
    return this.countries.slice();
  }

  fetchCountry(code: string){
    return this.http.get<Country>(`http://146.185.154.90:8080/restcountries/rest/v2/alpha/${code}`).pipe(
      map(result => {
        console.log(result);
        return new Country(result.alpha3Code, result.name, result.region, result.population, result.capital, result.flag);

      })
    );
  }
  
}
