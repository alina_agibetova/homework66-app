import { Component, OnInit } from '@angular/core';
import { Country } from '../../shared/country.model';
import { ActivatedRoute } from '@angular/router';
import { CountryService } from '../../shared/country.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-country-item',
  templateUrl: './country-item.component.html',
  styleUrls: ['./country-item.component.css']
})
export class CountryItemComponent implements OnInit{
  country!: Country;

  constructor(private route: ActivatedRoute, private countryService: CountryService, private http: HttpClient) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      console.log(data);
      this.country = <Country>data.country;

    })
  }
}
