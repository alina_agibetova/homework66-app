import { Injectable } from '@angular/core';
import { CountryService } from '../shared/country.service';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Country } from '../shared/country.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<Country>{


  constructor(private countryService: CountryService, private http: HttpClient) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country> {
    const countryId = <string>route.params['alpha3Code'];
    console.log(countryId);
    return this.countryService.fetchCountry(countryId);
  }
}
