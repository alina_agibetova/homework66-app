import { Component, OnDestroy, OnInit } from '@angular/core';
import { Country } from '../shared/country.model';
import { Subscription } from 'rxjs';
import { CountryService } from '../shared/country.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit, OnDestroy {
  countries!: Country[];
  countriesChangeSubscription!: Subscription;
  countriesFetchingSubscription!: Subscription;
  isFetching: boolean = false;

  constructor(private countryService: CountryService, private http: HttpClient) { }

  ngOnInit(): void {
    this.countries = this.countryService.getCountries();
    this.countriesChangeSubscription = this.countryService.countryChange.subscribe((countries: Country[])=> {
      this.countries = countries;
    });

    this.countriesFetchingSubscription = this.countryService.countryFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
      console.log(this.isFetching);
    });

    this.countryService.fetchCountries();
  }

  ngOnDestroy(): void {
    this.countriesChangeSubscription.unsubscribe();
    this.countriesFetchingSubscription.unsubscribe();
  }

}
